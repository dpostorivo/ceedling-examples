/************************************************************************//**
 *
 * \file test_BasicOperations.c
 *
 * \brief Test scripts for module BasicOperations to be run with Unity
 *
 * \note
 *
 * \author dom (BuLogics, Inc.)
 * \date 2016-03-30
 *
 ****************************************************************************/

/****************************************************************************
 *                              INCLUDE FILES                               *
 ****************************************************************************/
#include "unity.h"
#include "BuMath.h"
#include "mock_BuStatistics.h"

/****************************************************************************
 *                      PRIVATE TYPES and DEFINITIONS                       *
 ****************************************************************************/

/****************************************************************************
 *                              PRIVATE DATA                                *
 ****************************************************************************/

/****************************************************************************
 *                             EXTERNAL DATA                                *
 ****************************************************************************/

/****************************************************************************
 *                     PRIVATE FUNCTION DECLARATIONS                        *
 ****************************************************************************/

/****************************************************************************
 *                     EXPORTED FUNCTION DEFINITIONS                        *
 ****************************************************************************/

void setUp(void)
{
}

void tearDown(void)
{
}

void testAddWorks(void)
{
    uint8_t x = 1, y = 2, z;

    TEST_ASSERT_EQUAL( BUMATH_STATUS_SUCCESS, BUMATH_Add(x, y, &z));
    TEST_ASSERT_EQUAL(3,  z);

    x = 50;
    y = 70;
    TEST_ASSERT_EQUAL( BUMATH_STATUS_SUCCESS, BUMATH_Add(x, y, &z));
    TEST_ASSERT_EQUAL(120,  z);

    x = 100;
    y = 200;
    TEST_ASSERT_EQUAL(BUMATH_STATUS_SUCCESS, BUMATH_Add(x, y, &z));
    TEST_ASSERT_EQUAL(300,  z);


}

void testAddArrayElements(void)
{
    uint8_t testArray[] = {1, 2, 3, 4};
    uint8_t sum;

    TEST_ASSERT_EQUAL(BUMATH_STATUS_SUCCESS, BUMATH_AddArrayElements(testArray, sizeof(testArray), &sum));
    TEST_ASSERT_EQUAL(10,  sum);


}
/****************************************************************************
 *                     PRIVATE FUNCTION DEFINITIONS                         *
 ****************************************************************************/

/************************************************************************//**
 * \brief
 * \param
 * \return
 ****************************************************************************/

// End of file.
