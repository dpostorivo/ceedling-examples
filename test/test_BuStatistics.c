/************************************************************************//**
 *
 * \file test_BuStatistics.c
 *
 * \brief Test scripts for module BuStatistics to be run with Unity
 *
 * \note
 *
 * \author dom (BuLogics, Inc.)
 * \date 2016-03-31
 *
 ****************************************************************************/

/****************************************************************************
 *                              INCLUDE FILES                               *
 ****************************************************************************/
#include "unity.h"
#include "BuStatistics.h"

/****************************************************************************
 *                      PRIVATE TYPES and DEFINITIONS                       *
 ****************************************************************************/

/****************************************************************************
 *                              PRIVATE DATA                                *
 ****************************************************************************/

/****************************************************************************
 *                             EXTERNAL DATA                                *
 ****************************************************************************/

/****************************************************************************
 *                     PRIVATE FUNCTION DECLARATIONS                        *
 ****************************************************************************/

/****************************************************************************
 *                     EXPORTED FUNCTION DEFINITIONS                        *
 ****************************************************************************/

void setUp(void)
{
}

void tearDown(void)
{
}

void testBuStatisticsShouldDoSomething(void)
{
    TEST_FAIL_MESSAGE("BuStatistics has no tests!");
}

/****************************************************************************
 *                     PRIVATE FUNCTION DEFINITIONS                         *
 ****************************************************************************/

/************************************************************************//**
 * \brief
 * \param
 * \return
 ****************************************************************************/

// End of file.
